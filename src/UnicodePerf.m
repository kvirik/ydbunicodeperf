UnicodePerf	; Unicode string functions vs. 8Bit string functions prerformance testing. *** Full free for use and modify ***.
	do TestExtractAtEnd
	do TestExtractAtBegin
	do TestPieceAtEnd
	do TestPieceAtBegin
	do TestTranslate
	do TestFind
	quit

	; Test $EXTRACT function. Extract symbols at end of pattern.
TestExtractAtEnd
	new delim,symbol,length,pattern,iterCount,count,startTime,finishTime,str,modeText
	do InitTestValues
	do TestHeader("$EXTRACT",pattern,iterCount,"Extract 100 symbols at end of pattern")
	;
	set startTime=$zut
	for count=1:1:iterCount set str=$extract(pattern,31900,32000)
	set finishTime=$zut
	;
	do TestFooter(finishTime,startTime,iterCount,forTime)
	quit

	; Test $EXTRACT function. Extract symbols at begin of pattern.
TestExtractAtBegin
	new delim,symbol,length,pattern,iterCount,count,startTime,finishTime,str,modeText
	do InitTestValues
	do TestHeader("$EXTRACT",pattern,iterCount,"Extract 100 symbols at begin of pattern")
	;
	set startTime=$zut
	for count=1:1:iterCount set str=$extract(pattern,1,100)
	set finishTime=$zut
	;
	do TestFooter(finishTime,startTime,iterCount,forTime)
	quit

	; Test $PIECE function. Extract last field at end of pattern.
TestPieceAtEnd
	new delim,symbol,length,pattern,iterCount,count,startTime,finishTime,str,modeText,field
	do InitTestValues
	do TestHeader("$PIECE",pattern,iterCount,"Extract last field at end of pattern")
	;
	set field=$length(pattern,delim)
	set startTime=$zut
	for count=1:1:iterCount set str=$piece(pattern,delim,field)
	set finishTime=$zut
	;
	do TestFooter(finishTime,startTime,iterCount,forTime)
	quit

	; Test $PIECE function. Extract first field at begin of pattern.
TestPieceAtBegin
	new delim,symbol,length,pattern,iterCount,count,startTime,finishTime,str,modeText
	do InitTestValues
	do TestHeader("$PIECE",pattern,iterCount,"Extract first field at begin of pattern")
	;
	set startTime=$zut
	for count=1:1:iterCount set str=$piece(pattern,delim,1)
	set finishTime=$zut
	;
	do TestFooter(finishTime,startTime,iterCount,forTime)
	quit

	; Test $TRANSLATE function. Replace delimiters in pattern.
TestTranslate
	new delim,symbol,length,pattern,iterCount,count,startTime,finishTime,str,modeText
	do InitTestValues
	set iterCount=100000  ;__translate is slow in 8Bit mode too
	do TestHeader("$TRANSLATE",pattern,iterCount,"Replace delimiters in pattern")
	;
	set startTime=$zut
	for count=1:1:iterCount set str=$translate(pattern,delim,"^")
	set finishTime=$zut
	;
	do TestFooter(finishTime,startTime,iterCount,forTime)
	quit

	; Test $FIND function. Find first delimiter in pattern.
TestFind
	new delim,symbol,length,pattern,iterCount,count,startTime,finishTime,str,modeText
	do InitTestValues
	do TestHeader("$FIND",pattern,iterCount,"Find first delimiter in pattern")
	;
	set startTime=$zut
	for count=1:1:iterCount set str=$find(pattern,delim)
	set finishTime=$zut
	;
	do TestFooter(finishTime,startTime,iterCount,forTime)
	quit


InitTestValues
	new item,tmp
	;__Delimiter. Same symbol for 8Bit mode Cyrillic "■" (CP866 char 254) and Unicode mode Cyrillic "■" (char 9632)
	set delim=$select($zchset="UTF-8":$char(9632),1:$zchar(254))
	;__Unicode and 8Bit symbols for pattern. Same symbol for 8Bit mode Cyrillic "Ш" (CP866 char 152) and Unicode mode Cyrillic "Ш" (char 1064)
	set symbol=$select($zchset="UTF-8":$char(1064),1:$zchar(152))
	;__Pattern length for test. 32000 symbols in Unicode mode and 64000 symbols in 8Bit mode (64000 bytes in both cases)
	set length=$select($zchset="UTF-8":32000,1:64000)
	;__Pattern string for test.
	set pattern=$translate($justify("",length)," ",symbol)
	;__Insert delimiters between each 1000 symbols
	for item=1:1000:$length(pattern) set $extract(pattern,item)=delim
	;__Iterations count. One hundred thousand for Unicode mode (bigger value causes too long test). Ten million for 8Bit mode
	set iterCount=$select($zchset="UTF-8":100000,1:10000000)
	;__Time of FOR loop without SET operation
	set forTime=$zut for item=1:1:iterCount ;set tmp=""
	set forTime=$zut-forTime
	;__Done
	quit


TestHeader(func,pattern,iterCount,typeText)
	new modeText
	set modeText=$select($zchset="UTF-8":"Unicode mode",1:"8Bit mode")
	write !
	write $translate($justify("",80)," ","="),!
	write "Test "_func_" function for "_modeText_" on pattern length: "_$length(pattern)_" symbols.",!
	write typeText_". "_$fn(iterCount,",")_" iterations.",!
	quit


TestFooter(finishTime,startTime,iterCount,forTime)
	write "Elapsed time: "_$fn(((finishTime-startTime)-forTime)/1000000,"",4)_" seconds",!
	write "Single operation time: "_$$SingleOperTimeFmt(finishTime,startTime,iterCount,forTime),!
	write $translate($justify("",80)," ","="),!
	quit


SingleOperTimeFmt(finishTime,startTime,iterCount,forTime)
	new text,diff,opTime
	set diff=(finishTime-startTime)-forTime
	set opTime=diff/iterCount
	set text=(opTime)_" microseconds ("_(opTime*1000)_" nanoseconds)"
	quit text


	; Results for 32k symbols strings
	; +-------------+------------+-----------+------------------+
	; |         Mode|    8Bit    |  Unicode  |    Difference    |
	; +-------------|            |           |  in percents, %  |
	; |Function     |            |           |                  |
	; +-------------+------------+-----------+------------------+
	; |$E() at      |  0.0048370 | 133.67991 |        2,763,694 |
	; |string end   |         μs |        μs |    it's too big! |
	; +-------------+------------+-----------+------------------+
	; |$E() at      |  0.0047030 |   0.09340 |            1,985 |
	; |string begin |         μs |        μs |    it's big too! |
	; +-------------+------------+-----------+------------------+
	; |$P() at      |  0.0132654 |   0.01752 |               75 |
	; |string end   |         μs |        μs |  it's acceptable |
	; +-------------+------------+-----------+------------------+
	; |$P() at      |  0.0084794 |   0.01400 |               60 |
	; |string begin |         μs |        μs |  it's acceptable |
	; +-------------+------------+-----------+------------------+
	; |$TR()        | 43.9664300 | 207.77973 |               21 |
	; |             |         μs |        μs |      nearly same |
	; +-------------+------------+-----------+------------------+
	; |$F()         |  0.0083502 |   0.02095 |               40 |
	; |             |         μs |        μs |  it's acceptable |
	; +-------------+------------+-----------+------------------+
