# YDBUnicodePerf

## YottaDB. Unicode and 8Bit string functions performance comparison.

During porting of my project from 8-bit encoding to Unicode, I detected a slowdown of some functions.
I wrote a small utility to check the speed of the basic string processing functions.
After running on an 8-bit ($zchset="M") configuration and on a Unicode configuration ($zchset="UTF-8"), I found very strange results.
The function $extract on medium length strings (~32k) can be slowed down by tens thousands of times in the $zchset="UTF-8" mode.
This directrly depends on the length of the string being processed.
Such results seemed wery surprising to me.
I understand that for processing characters in Unicode mode, it is necessary to do character-by-character string scanning.
But it seems that this should not give such extremely strong slowdown.

## Utility invoke:

**do ^UnicodePerf**

Results for 32k symbols strings

|Mode|8Bit|Unicode|Difference in percents, % |
|----|----|-------|--------------------------|
|$E() at string end|0.0048370 μs|133.67991 μs|2,763,694 (it's too big!)|
|$E() at string begin|0.0047030 μs|0.09340 μs|1,985 (it's big too!)|
|$P() at string end|0.0132654 μs|0.01752 μs|75 (it's acceptable)|
|$P() at string begin|0.0084794 μs|0.01400 μs|60 (it's acceptable)|
|$TR()|43.9664300 μs|207.77973 μs|21 (nearly same)|
|$F()|0.0083502 μs|0.02095 μs|40 (it's acceptable)|

